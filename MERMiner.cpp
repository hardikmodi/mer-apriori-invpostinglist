#include "MERMiner.h"
#include <fstream>
#include <string.h>
#include<iostream>
#include "SetOfSet.h"
#include "time.h"
#include "InvPostingList.h"

MERMiner::MERMiner(Dataset& d) : ds(d)
{ 
    countGetNextCandidateItemset=0;
    countGetSupportCount=0;
    countGetNextRule=0; 
    countCalculateConfidence=0;
    timeGetNextCandidateItemset=0;
    timeGetSupportCount=0;
    timeGetNextRule=0;
    timeCalculateConfidence=0;
    timeSubsetCheck=0;
    timePhase1=0;
}
    
void MERMiner::getNextCandidateItemset(void)
{
    countGetNextCandidateItemset++;
    static long int last=ds.no_of_items -1 ;
    static long int current=last-1;      
    static set<int>::iterator *pts =NULL;
    static set<int> workingSetCopy;
    if (current<0) // no more candidate sets to generate
    {
        candidate.clear();  
        workingSetCopy.clear();
        if (NULL!=pts)
                delete [] pts;
        pts=NULL;
    }
    else
    {
        //the last set generated was one more than the current, this is also true for the starting point
        if (last>current) // start of a new level
        {
            if (NULL!=pts)
                delete [] pts;                        
            pts = new set<int>::iterator[current+1];
            workingSetCopy.clear();
            candidate.clear(); 
            if (Freq.cardinality()<=0)
            {    
                workingSetCopy = ds.items;
            }
            else
            {                
                if (workingSet.size()==0)
                {
                    current=0;
                    if (NULL!=pts)
                        delete [] pts;
                    return;
                }
                workingSetCopy=workingSet;
            }
            workingSet.clear();
            set<int>::iterator witr=workingSetCopy.begin();
            for(int i=0;i<=current;i++,witr++)
            {
                pts[i]=witr;                      
                candidate.insert(*pts[i]);
            }
            last=current;
        }
        else
        {
            set<int>::iterator oneLessThanLast = workingSetCopy.end();
            oneLessThanLast--;
            if (pts[current] == oneLessThanLast) // last pointer reached the end of list 
            {                   
                set<int>::iterator p_itr,q_itr,temp;
                long int x=current;
                q_itr=workingSetCopy.end();
                q_itr--;                
                if (workingSetCopy.size()>1)
                {
                    p_itr=q_itr;
                    p_itr--;                
                }
                while(((x>0)&&(p_itr!=workingSetCopy.begin())&&pts[x]==q_itr)&&(pts[x-1]==p_itr))
                {
                    p_itr--;
                    q_itr--;
                    x--;
                }
                if ((p_itr==workingSetCopy.begin())||(x==0))
                {                    
                    current--;                   
                    getNextCandidateItemset();
                }
                else
                {                    
                    candidate.erase(*pts[x-1]);
                    pts[x-1]++;
                    candidate.insert(*pts[x-1]);
                    for (int i=x;i<=current;i++)
                    {
                        candidate.erase(*pts[i]);
                        pts[i]=pts[i-1];
                        pts[i]++;
                        candidate.insert(*pts[i]);
                    }
                }
            }
            else
            {               
                candidate.erase(*pts[current]);
                pts[current]++;
                candidate.insert(*pts[current]);
            }
        }
    }    
}

float MERMiner::getSupportCount()
{
    countGetSupportCount++;
    float sup=0;
    bool flag;
    set<int>::iterator c_itr;
    for(int i=0;i<ds.no_of_transactions;i++)
    {
        flag=true;
        for(c_itr=candidate.begin();(flag)&&(c_itr!=candidate.end());c_itr++)
        {
            if (ds.ibm[(*c_itr)][i]==true)
            {
                sup++;
                flag=false;
            }
        }
    }
    return (sup/ds.no_of_transactions);
}

void MERMiner::getNextRule(void)
{    
    countGetNextRule++;
    if (Freq.cardinality()<=1)
    {
        X.clear();
        Y.clear();
        return;
    }
    else
    {
        static set<set<int> >::iterator x_itr=Freq.first();
        static set<set<int> >::iterator y_itr = Freq.first();    
        static set<set<int> >::iterator last;
        static set<set<int> >::iterator sec_last;        
        last = Freq.last();        
        sec_last=last;
        sec_last--;
        if ((y_itr==last))
        {
            if (x_itr==sec_last)
            {                
                X.clear();
                Y.clear();
                return;
            }
            else
            {                
                x_itr++;
                y_itr=x_itr;
                y_itr++;           
            }
        }
        else
        {            
            y_itr++;
        }
        
        if ((y_itr!=Freq.last()))
        {            
            if (Freq.isIntersection((*x_itr),(*y_itr)))
            {                
                getNextRule();
            }
            else
            {                
                X.clear();
                Y.clear();
                X=(*x_itr);
                Y=(*y_itr);
            }             
        }
        else
        {
            getNextRule();
        }
    }
}

float MERMiner::calculateConfidence(void)
{
    countCalculateConfidence++;
    float supp_x,supp_y,supp_x_y;
    supp_x=FreqSupp[X];
    supp_y=FreqSupp[Y];
    set<int> Z=X;
    Z.insert(Y.begin(),Y.end());
    supp_x_y=FreqSupp[Z];
    if (Z.size()==ds.no_of_items)
        supp_x_y=1;
    return ((2) - ((supp_x+supp_y)/supp_x_y));
}

long int MERMiner::mine(char * o_path, float m_sup, float m_conf)
{
    clock_t start,end;
    InvPostingList MaxInFreq(ds.no_of_items );
    float supp,conf;   
    long int count_rules=0;
    ofstream outfile(o_path,ios::binary);
    timePhase1=clock();
    start=clock();
    getNextCandidateItemset();        
    end=clock();
    timeGetNextCandidateItemset+=end-start;
    bool maxflag=false;        
    cout<<"\n==========Mining with below parameters==========";
    cout<<"\nSupport Threshold: "<<m_sup;
    cout<<"\nConfidence Threshold: "<<m_conf;    
    while (candidate.size()>0)
    {                 
        start=clock();
        maxflag=MaxInFreq.subsetCheck(candidate);
        end=clock();
        timeSubsetCheck+=(end-start);
        if (!(maxflag))
        {
            start=clock();
            supp=getSupportCount();
            end=clock();
            timeGetSupportCount+= end- start;
            if (supp>=m_sup)
            {
                Freq.add(candidate);
                updateWorkingSet();
                FreqSupp[candidate]=supp;
            }
            else                
                MaxInFreq.add(candidate);
        }         
        start=clock();
        getNextCandidateItemset(); 
        end=clock();
        timeGetNextCandidateItemset+=end-start;
    }     
    timePhase1=clock()-timePhase1;
    cout<<"\n\nNumber of Frequent Itemsets: "<<Freq.cardinality()<<" out of "<<(1<<(ds.no_of_items))<<"\n";
    cout<<"Number of Maximal InFrequent Itemsets: "<<MaxInFreq.cardinality()<<"\n";        
    count_rules=0;
    timeGetNextRule=0;
    if (Freq.cardinality()>0)
    {
        start=clock();
        getNextRule();
        end=clock();
        timeGetNextRule+=end-start;
        while((X.size()>0)&&(Y.size()>0))
        {
            start=clock();
            conf = calculateConfidence();
            end=clock();
            timeCalculateConfidence+=end-start;            
            if (conf>=m_conf)
            {                                
                writeRuleToFile(outfile,X,Y,conf);
                count_rules++;
            }
            start=clock();
            getNextRule();
            end=clock();
            timeGetNextRule+=end-start;
        }        
    }
    else
        outfile.write("No Freq Itemsets",strlen("No Freq Itemsets"));
    cout<<"\nNumber of MER Rules: "<<count_rules;                
    outfile.flush();
    outfile.close();
    return count_rules;
}
long int MERMiner::writeRuleToFile(ofstream& out, set<int> a,set<int> b)
{
    set<int>::iterator itr;
    out<<"{";
    for (itr=a.begin();itr!=a.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"} exclusive {";
    for (itr=b.begin();itr!=b.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"}\n";
}

long int MERMiner::writeRuleToFile(ofstream& out, set<int> a,set<int> b,float confval)
{
    set<int>::iterator itr;
    out<<"{";
    for (itr=a.begin();itr!=a.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"} exclusive {";
    for (itr=b.begin();itr!=b.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"} \t"<<confval<<" \n";
}

void MERMiner::showCandidate(void)
{
    set<int>::iterator itr;
    cout<<"\nX:";
    for(itr=candidate.begin();itr!=candidate.end();itr++)
    {
        cout<<(*itr);
    }           
}

void MERMiner::showRule()
{
    set<int>::iterator itr;
    cout<<"\nX:";
    for(itr=X.begin();itr!=X.end();itr++)
    {
        cout<<(*itr);
    }            
    cout<<"\tY::";
    for(itr=Y.begin();itr!=Y.end();itr++)
    {
        cout<<(*itr);
    }            

}

void MERMiner::updateWorkingSet(void)
{
    if (workingSet.size() >= ds.no_of_items)
        return;
    else
    {
        set<int>::iterator c_itr=candidate.begin();
        for (;c_itr!=candidate.end();c_itr++)
        {
            workingSet.insert(*c_itr);
        }      
    }
}

