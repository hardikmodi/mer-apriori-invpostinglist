/* 
 * File:   InvPostingList.h
 * Author: aeon
 *
 * Created on 27 January, 2013, 3:31 PM
 */

#ifndef INVPOSTINGLIST_H
#define	INVPOSTINGLIST_H
#include <list>
#include <set>

using namespace std;

class InvPostingList {
private:   
        int no_of_items;        
        list<int> ** ipos;
        int countMaxInFreq;
        list<int>::iterator *ptrs;
public:
    InvPostingList(int);
    void add(set<int> &);
    bool subsetCheck(set<int> &);
    long int cardinality(void);
    virtual ~InvPostingList();


};

#endif	/* INVPOSTINGLIST_H */

