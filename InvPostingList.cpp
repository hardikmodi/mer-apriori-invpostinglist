/* 
 * File:   InvPostingList.cpp
 * Author: aeon
 * 
 * Created on 27 January, 2013, 3:31 PM
 */

#include "InvPostingList.h"

#include<list>
#include<set>
#include <stdlib.h>

using namespace std;

InvPostingList::InvPostingList(int n)
{
	no_of_items = n;	
	countMaxInFreq = 0;
        ipos = new list<int> *[no_of_items];                
	for(int i=0;i<no_of_items;i++)
	{		
		ipos[i]=NULL;
	}
        ptrs = new list<int>::iterator[no_of_items];
}

void InvPostingList::add(set<int> & s)
{
	set<int>::iterator itr;
	for (itr=s.begin();itr!=s.end();itr++)
	{
		if (ipos[(*itr)]==NULL)
		{
			ipos[(*itr)]= new list<int>;
		}
		(*ipos[(*itr)]).push_back(countMaxInFreq);		
	}
	countMaxInFreq++;
}

long int InvPostingList::cardinality(void)
{
	return countMaxInFreq;
}

bool InvPostingList::subsetCheck(set<int> &s)
{
	int simCount=0;
	int n=s.size();
	int max=0;	
	set<int>::iterator itr;
	simCount=0;
	for (itr=s.begin();itr!=s.end();itr++)
	{
		if (ipos[(*itr)]==NULL)
                {                    
                    return false;
                }
		ptrs[(*itr)]=(*ipos[(*itr)]).begin();
		if ((*ptrs[(*itr)]) > max)
			max=*ptrs[(*itr)];
	}
	while(simCount<n)
	{
		simCount=0;
		for(itr=s.begin();itr!=s.end();itr++)
		{
			while(((*ptrs[(*itr)]) < max) && (ptrs[(*itr)]!=(*ipos[(*itr)]).end()))
			{
				ptrs[(*itr)]++;
			}
			if (ptrs[(*itr)] == (*ipos[(*itr)]).end())
				return false;
			if (*ptrs[(*itr)] == max)
				simCount++;
		}
		max=0;
		for (itr=s.begin();itr!=s.end();itr++)
		{							
			if ((*ptrs[(*itr)]) > max)
				max=*ptrs[(*itr)];
		}
	}
	if(simCount == n)
        {
                return true;
        }
		
}







InvPostingList::~InvPostingList() {
    for (int i =0;i<no_of_items;i++)
    {
        if (ipos[i]!=NULL)
        {
            (*ipos[i]).clear();
            delete ipos[i];
        }            
    }
    delete [] ipos;
    delete [] ptrs;
}

